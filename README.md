# ADD-ONS I USE FOR WOW CLASSIC

___

## [Interface](#interface-1)
## [Combat](#combat-1)
## [Utility](#utility-1)
___

### Interface
- [Luna Unit Frames](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/Luna/) : barre de vie, groupe, raid, target

___

- [OmniCC timer](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/OmniCC/) : affiche le temps de recharge restant CD
- [LeatrixMap](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/LeatrixMap) : permet d'afficher les zones de map non explorées et les points de fly
- [SexyMap](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/SexyMap) : permet d'améliorer la mini-map, la taille le style, etc

___

## Combat  

- [ABarClassic](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/AbarClassic) : permet d'afficher les délais d'auto-attaque de l'ennemi et de soi-même
- [RealMobHealth](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/RealMobHealth) : permet d'afficher les points de vie des monstres (et pas seulement le %)
- [Classic Thresat Meter](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/ClassicThreatMeter) : affiche la menace pour chaque joueur
- [Classic Cast Bar](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/CastBar) : affiche les barres de sorts à l'ancienne

___ 

## Utility
- [Auctioneer](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/Auctioneer) : un hv amélioré, très customisable
- [BagFreeSpaceCounter](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/BagFreeSpaceCounter/) : afficher l'espace restant dans les sacs
- [Chat Link Icons](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/ChatLinkIcons) : afficher l'icône de classe du joueur dans le chat
- [Details](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/Details) : analyse de logs du combat
- [Let Me Cast](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/LetMeCast) : si on est assis/en monture, permet de dismount/de se lever lorsqu'on essaie de lancer un sort 
- [Questie](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/Questie) : QoL pour les quêtess
- [Vendor Price](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/VendorPrice) : affiche le prix de vente au PNJ
- [Field Guide](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/FieldGuide) : indique les talents à acheter pour chaque classe et chaque level
- [Gatherer](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/GatherMate) : pour les récoltes, stock tous les endroits où on récolte poiur optimiser le farming
- [AtlasLoot](https://gitlab.com/flurrylol/wowclassic-addon/tree/master/AtlasLoot) : pour regarder ce qu'on peut ninja
