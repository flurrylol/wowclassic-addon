## Interface: 80200
## Title: ChatLinkIcons v1.7
## Version: 1.7
## Author: SDPhantom
## OptionalDeps: Pawn
## SavedVariables: ChatLinkIcons_Options
## Notes: Displays icons for links in chat.

Options.lua
Core.lua
