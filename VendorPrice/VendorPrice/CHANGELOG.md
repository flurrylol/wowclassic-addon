# Vendor Price

## [1.2.1](https://github.com/ketho-wow/VendorPrice/tree/1.2.1) (2019-09-06)
[Full Changelog](https://github.com/ketho-wow/VendorPrice/compare/1.2.0...1.2.1)

- Shift now displays the individual item price only  
    No longer disables Auctionator vendor price tooltip  
