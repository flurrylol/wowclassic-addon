# GatherMate2

## [1.44.4-3-g7907d17](https://github.com/Nevcairiel/GatherMate2/tree/7907d1776f9dc16fe26fbca9b0408b11a1e2d786) (2019-08-12)
[Full Changelog](https://github.com/Nevcairiel/GatherMate2/compare/1.44.4...7907d1776f9dc16fe26fbca9b0408b11a1e2d786)

- Disable WoWI upload for classic, and explicitly flag build version  
- Merge branch 'master' into classic  
- Preliminary support for WoW Classic  
