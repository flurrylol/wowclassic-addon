local AL = _G.AtlasLoot.GetLocales("deDE")

if not AL then return end

-- These localization strings are translated on Curseforge: https://www.curseforge.com/wow/addons/atlaslootclassic/localization
-- Collections
AL["%s Sets"] = "%s Sets"
AL["Childrens Week"] = "Kinderwoche"
AL["First Prize"] = "Erster Preis"
AL["One-Handed Weapons"] = "Einhandwaffen"
AL["Ranged Weapons"] = "Fernwaffen"
AL["Rare Fish"] = "Seltener Fisch"
AL["Rare Fish Rewards"] = "Belohnungen für seltene Fische"
AL["Scourge Invasion"] = "Invasion der Geißel"
AL["The Duke of Cynders"] = "Der Fürst der Asche"
AL["The Duke of Fathoms"] = "Der Fürst der Tiefen"
AL["The Duke of Shards"] = "Der Fürst der Splitter"
AL["The Duke of Zephyrs"] = "Der Fürst der Stürme"
AL["Tier Sets"] = "Tier Sets"
AL["Two-Handed Weapons"] = "Zweihandwaffen"
AL["Wands"] = "Zauberstäbe"
AL["World Events"] = "Weltereignisse"

