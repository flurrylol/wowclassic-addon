# AtlasLootClassic

This mod is distributed under Version 2 of the GPL.  A copy of the GPL is included in this zip file with links to non-english translations.

[Changelog history](https://github.com/Hoizame/AtlasLootClassic/blob/master/AtlasLootClassic/Documentation/Release_Notes.md)

## v1.1.6-beta (Aug. 25, 2019)

- Big thanks for the translation work at curseforge!
- Favourites tooltip icon and lists can now enabled separately
- Add faction mounts, some have bad modelzoom but can't fix that now
- Cleanup Phase 2/3 items with new info from blizzards AmA
