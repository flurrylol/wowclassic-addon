local AL = _G.AtlasLoot.GetLocales("deDE")

if not AL then return end

-- These localization strings are translated on Curseforge: https://www.curseforge.com/wow/addons/atlaslootclassic/localization
-- Crafting
AL["Axes"] = "Äxte"
AL["Class Professions"] = "Klassenspezifische Berufe"
AL["Enhancements"] = "Verbesserungen"
AL["Fireworks"] = "Feuerwerk"
AL["Maces"] = "Kolben"
AL["Oil"] = "Öl"
AL["Pets"] = "Haustiere"
AL["Swords"] = "Schwerter"

